module.exports = {
	entry: './main.js',
	output: {
		filename: './bundle.js'
	},
	watch: true,
	module: {
		rules: [
			{
				test: /\.css$/,
				use: [
					{ loader: "style-loader" },
					{ loader: "css-loader" }
				]
			}, {
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['env']
					}
				}
			},
			{
				test: /\.(jpe?g|png|woff|woff2|eot|ttf|gif|svg)$/i,
				use: [
					'url-loader?limit=10000',
					'img-loader'
				]
			}]
	}
};